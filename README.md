# Carcassonne #

## Author ##
Max Goad

## Date ##
Summer 2017

## Summary ##
This repository houses a games project that I am currently working on for the summer of 2017. Its original intent was to replicate the popular board game "Carcassonne" in Unity, and once complete modify it enough to be able to release on distribution platforms to be determined. I believe this project to be a worthwhile addition to my resume once complete, and hope it can contribute towards my junior introduction into the game development industry!

## Technology/Languages Used ##
* Unity 5.6.1
* C#
* XML

## Contact Information ##
Anyone who wishes to contact me can do so via the contact information found on my personal website below. This is also where you can find lists of my scholastic and work experience, along with more personal projects and updated resumes.

[www.maxgoad.com](http://www.maxgoad.com/)

## TODO LIST ##

### Soon ###

* Implement Game Scene initial load data override (to avoid main menu for testing)
* Implement Scoring
	* Tracking of ScoreMarkers
	* Algorithmic Logic
	* Display of some sort
* Implement ownership of tile groups based on algorithms used for scoring
* Implement pre and post game
	* Pre
		* Main Menu
			* Hook up Buttons properly (3/4 complete)
		* Custom Tilesets Screen
			* Build scene
			* Tileset selection
			* Tileset creation
			* Saving and loading tilesets
	* Post
		* Scoreboard

### Later ###

* Implement placing marker(s) only on tile you placed? (Gameplay decision)
* Implement way to tell if a tile is impossible to play (+handling)
* Add way to see all remaining tiles and scoremarkers during game
* Implement farms
* Saving and loading mid-game
* Redo all visual elements?
	* Add game board
* Add audio to the game
	* Finish Audio Setting in Settings Screen
* Refactor extremely janky main menu (especially play game and related screens)