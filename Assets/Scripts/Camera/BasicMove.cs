﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicMove : MonoBehaviour
{
	[Range(1.0f, 10.0f)]
	public float speed = 5.0f;

	[MinMaxRange(ZOOM_MIN, ZOOM_MAX)]
	public MinMaxRange zoomMinMax = new MinMaxRange(ZOOM_MIN, ZOOM_MAX);

	private const float ZOOM_MIN = -5.0f;
	private const float ZOOM_MAX = 10.0f;

	#region Unity Functions

	public void Update() {
		ProcessLateralMovement();
		ProcessDepthMovement();
	}

	private void ProcessLateralMovement() {
		Move(Vector3.right * Input.GetAxis("Camera Horizontal"));
		Move(Vector3.up * Input.GetAxis("Camera Vertical"));
	}

	#endregion

	#region Public Functions

	#endregion

	#region Private Functions

	private void ProcessDepthMovement() {
		float zoomDelta = Input.GetAxis("Camera Zoom");
		Move(Vector3.forward * zoomDelta);
		ClampZValue();
	}

	private void Move(Vector3 direction) {
		
		transform.position += (direction * speed / 10);

	}

	private void ClampZValue() {
		if (transform.position.z < zoomMinMax.rangeStart) {
			transform.position = new Vector3(transform.position.x, transform.position.y, zoomMinMax.rangeStart);
		}
		else if (transform.position.z > zoomMinMax.rangeEnd) {
			transform.position = new Vector3(transform.position.x, transform.position.y, zoomMinMax.rangeEnd);
		}
	}

	#endregion
}
