﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoRotate : MonoBehaviour
{
	public Axis rotationAxis = Axis.Y;

	[Range(-10.0f, 10.0f)]
	public float speed;

	public void Update() {
		switch (rotationAxis) {
		case Axis.X:
			transform.Rotate(Vector3.left, speed * Time.deltaTime);
			break;
		case Axis.Y:
			transform.Rotate(Vector3.up, speed * Time.deltaTime);
			break;
		case Axis.Z:
			transform.Rotate(Vector3.forward, speed * Time.deltaTime);
			break;
		}
	}

}

public enum Axis
{
	X,
	Y,
	Z
}
