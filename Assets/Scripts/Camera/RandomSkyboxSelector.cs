﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSkyboxSelector : MonoBehaviour
{
	public List<Material> customSkyboxes;

	#region Unity Functions

	public void Awake() {
		if (customSkyboxes != null && customSkyboxes.Count > 0) {
			RenderSettings.skybox = customSkyboxes[Random.Range(0, customSkyboxes.Count)];
		}
	}
	
	public void Update() {
		
	}

	#endregion

}
