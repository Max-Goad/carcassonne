﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmoothFollow : MonoBehaviour
{
	public GameObject followObject;

	[Range(0.1f, 1.0f)]
	public float followDelay = 0.125f;

	[Range(-30.0f, 0.0f)]
	public float offsetHeight = -10.0f;

	private Vector3 offset;

	#region Unity Functions

	public void Awake() {
		offset = new Vector3(0.0f, 0.0f, offsetHeight);
	}

	public void Update() {
		transform.position = Vector3.Lerp(transform.position,
		                                  followObject.transform.position + offset,
		                                  followDelay);
	}

	#endregion
}
