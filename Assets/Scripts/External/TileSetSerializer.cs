﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using UnityEngine;

public class TileSetSerializer
{
	public static string XML_TILESET_PATH = Application.streamingAssetsPath + "/XML/TileSets/";

	private XmlSerializer serializer;

	#region Singleton Data

	private static TileSetSerializer instance;
	public static TileSetSerializer Instance {
		get {
			if (instance == null) {
				instance = new TileSetSerializer();
			}
			return instance;
		}
	}

	private TileSetSerializer() {
		this.serializer = new XmlSerializer(typeof(SerializableTileSet));
	}

	#endregion

	public void SaveAllTileSets(List<TileSet> tileSetsToSave) {
		foreach (TileSet each in tileSetsToSave) {
			Debug.Log("Saving tileset: " + each);
			SaveTileSet(each);
		}
	}

	public void SaveTileSet(TileSet tileSetToSave) {
		SerializableTileSet sts = new SerializableTileSet(tileSetToSave);
		FileStream stream = new FileStream(XML_TILESET_PATH + sts.tileSetName + ".xml", FileMode.Create);
		serializer.Serialize(stream, sts);
		stream.Close();

		#if UNITY_EDITOR
		UnityEditor.AssetDatabase.Refresh();
		#endif
	}

	public List<TileSet> LoadAllTileSets() {
		List<TileSet> loadedList;
		try {
			loadedList = AttemptLoadAllTileSets();
		} catch (IOException e) {
			loadedList = CreateBackupTileSets(e);
		}
		return loadedList;
	}

	private List<TileSet> AttemptLoadAllTileSets() {
		List<TileSet> loadedList = new List<TileSet>();
		foreach (string each in System.IO.Directory.GetFiles(XML_TILESET_PATH)) {
			if (!each.EndsWith(".xml"))
				continue;
			Debug.Log("Loading tileset: " + each);
			loadedList.Add(LoadTileSet(each));
		}
		if (loadedList.Count == 0) {
			throw new FileNotFoundException("No xml files found at path: " + XML_TILESET_PATH);
		}
		return loadedList;
	}

	private List<TileSet> CreateBackupTileSets(IOException e) {
		Debug.LogWarning("TileSetSerializer.AttemptLoadAllTileSets() encountered an exception!\nFull Error Message:\n" + e.ToString());
		Debug.LogWarning("Creating default tileset and attempting to save...");

		TileSet defaultTileSet = TileSet.GetDefaultTileSet();
		List<TileSet> defaultSets = new List<TileSet>();
		defaultSets.Add(defaultTileSet);

		SaveTileSet(defaultTileSet);
		Debug.LogWarning("Default tileset saved to file successfully!");

		return defaultSets;
	}

	private TileSet LoadTileSet(string path) {
		FileStream stream = new FileStream(path, FileMode.Open);
		SerializableTileSet sts = serializer.Deserialize(stream) as SerializableTileSet;
		stream.Close();
		return sts.ToTileSet();
	}
}
