﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Marker : MonoBehaviour
{

	public SubTile subtilePlacedOn;

	private Player owner;

	public Player Owner {
		get { return owner; }
		set {
			owner = value;
			ChangeObjectColor();
		}
	}

	#region Unity Functions

	public void Awake() {
		
	}
	
	public void Update() {
		
	}

	#endregion

	#region Public Functions

	public int CalculateScore() {
		throw new UnityException("Unimplemented Function - CalculateScore");
	}

	#endregion

	#region Private Functions

	private void ChangeObjectColor() {
		this.gameObject.GetComponent<Renderer>().material.color = Owner.playerColor;
	}

	#endregion

}
