﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarkerPlacer : MonoBehaviour
{
	public delegate void MarkerPlacement();
	public static event MarkerPlacement SendMarkerPlaceEvent;

	private GameState gameState;
	private GameEngine gameEngine;
	private MarkerBag markerBag;
	private Marker placementMarker;

	#region Unity Functions

	public void Awake() {
		TagUtils.SafeTagOverride(this.gameObject, Tag.MarkerPlacer);
		gameState = TagUtils.GetTaggedObject(Tag.GameState).GetComponent<GameState>();
		gameEngine = TagUtils.GetTaggedObject(Tag.GameEngine).GetComponent<GameEngine>();
		markerBag = TagUtils.GetTaggedObject(Tag.MarkerBag).GetComponent<MarkerBag>();
	}
	
	public void Update() {
		MovePlacementMarker();
		CheckForInput();
	}

	#endregion

	#region Public Functions

	public void Enable() {
		if (placementMarker == null) {
			placementMarker = markerBag.GetMarker(gameState.CurrentPlayer);
		}
		this.enabled = true;
	}

	public void Disable() {
		if (placementMarker != null) {
			Debug.LogWarning("Disabling MarkerPlacer before its placement marker has been set to null!");
		}
		this.enabled = false;
	}

	#endregion

	#region Private Functions

	private void MovePlacementMarker() {
		Vector3 mousePosition = GetNormalizedMousePosition();
		VectorUtils.SnapToMultiple(ref mousePosition);
		mousePosition.z -= 0.5f;
		placementMarker.transform.position = mousePosition;
	}

	private Vector3 GetNormalizedMousePosition() {
		Vector3 mousePosition = Input.mousePosition;
		mousePosition.z -= Camera.main.transform.position.z;
		mousePosition = Camera.main.ScreenToWorldPoint(mousePosition);
		return mousePosition;
	}

	private void CheckForInput() {
		if (Input.GetButtonDown("Confirm")) {
			SubTile selectedSubTile = gameState.GetClosestSubTile(GetNormalizedMousePosition());
			PlaceMarkerOn(selectedSubTile);
		}
	}

	private void PlaceMarkerOn(SubTile subtile) {
		bool success = (subtile != null) && (gameEngine.PlaceMarkerOn(placementMarker, subtile));
		if (success) {
			OnPlacementSuccess();
		}
	}

	private void OnPlacementSuccess() {
		placementMarker = null;

		if (SendMarkerPlaceEvent != null) {
			SendMarkerPlaceEvent();
		}
	}

	#endregion

}
