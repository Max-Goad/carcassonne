﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarkerFactory : MonoBehaviour
{
	public Marker blankMarkerPrefab;

	#region Unity Functions

	public void Awake() {
		TagUtils.SafeTagOverride(this.gameObject, Tag.MarkerFactory);
	}

	#endregion

	#region Public Functions

	public Marker CreateMarker(Player player) {

		Marker newMarker = Instantiate(blankMarkerPrefab);

		newMarker.Owner = player;

		return newMarker;
	}

	#endregion

}
