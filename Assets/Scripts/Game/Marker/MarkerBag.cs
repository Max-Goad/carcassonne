﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarkerBag : MonoBehaviour
{

	private GameState gameState;
	private MarkerFactory markerFactory;

	private Dictionary<Player, Stack<Marker>> markerDictionary;

	#region Unity Functions

	public void Awake() {
		TagUtils.SafeTagOverride(this.gameObject, Tag.MarkerBag);
		gameState = TagUtils.GetTaggedObject(Tag.GameState).GetComponent<GameState>();
		markerFactory = TagUtils.GetTaggedObject(Tag.MarkerFactory).GetComponent<MarkerFactory>();

		markerDictionary = new Dictionary<Player, Stack<Marker>>();

		PopulateMarkers();
	}

	#endregion

	#region Public Functions

	public Marker GetMarker(Player player) {
		Marker marker = markerDictionary[player].Pop();
		marker.gameObject.SetActive(true);
		return marker;
	}

	public bool HasMarkersRemaining(Player player) {
		return markerDictionary[player].Count > 0;
	}

	#endregion

	#region Private Functions

	private void PopulateMarkers() {
		List<Player> players = gameState.Players;

		foreach (Player each in players) {
			Stack<Marker> newMarkers = new Stack<Marker>();
			for (int i = 0; i < gameState.MarkerLimit; i++) {
				newMarkers.Push(CreateMarker(each));
			}
			markerDictionary.Add(each, newMarkers);
		}
	}

	private Marker CreateMarker(Player player) {
		Marker newMarker = markerFactory.CreateMarker(player);
		newMarker.gameObject.name = player.playerName + " Marker";
		newMarker.transform.SetParent(this.transform);
		newMarker.gameObject.SetActive(false);
		return newMarker;
	}

	#endregion

}
