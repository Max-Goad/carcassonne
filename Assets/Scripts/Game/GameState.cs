﻿using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;

public class GameState : MonoBehaviour
{
	private TileBoard tileBoard;
	private List<Marker> placedMarkers;

	public int MarkerLimit {
		get;
		private set;
	}
	public Player CurrentPlayer {
		get;
		private set;
	}
	public TileSet CurrentTileSet {
		get;
		private set;
	}
	public List<Player> Players {
		get;
		private set;
	}

	#region Unity Functions

	public void Awake() {
		TagUtils.SafeTagOverride(this.gameObject, Tag.GameState);
		tileBoard = TagUtils.GetTaggedObject(Tag.TileBoard).GetComponent<TileBoard>();

		AttemptPersistantDataRetrieval();
		placedMarkers = new List<Marker>();
	}
	
	public void Update() {
		
	}

	#endregion

	#region Public Functions

	public void IncrementTurn() {
		int currentTurnIndex = Players.IndexOf(CurrentPlayer);
		if (++currentTurnIndex >= Players.Count) {
			currentTurnIndex = 0;
		}
		CurrentPlayer = Players[currentTurnIndex];
		Debug.Log(CurrentPlayer.playerName + " is now having their turn!");
	}

	public void PlaceTileAt(Tile newTile, Vector3 location) {
		tileBoard.PlaceTileAt(newTile, location);
	}

	public Tile GetTile(Vector3 location) {
		return tileBoard.GetTile(location);
	}

	public bool HasTileAt(Vector3 location) {
		return tileBoard.HasTileAt(location);
	}

	public bool HasAnyTiles() {
		return tileBoard.HasAnyTiles();
	}

	public SubTile GetClosestSubTile(Vector3 approxLocation) {
		Tile closestParentTile = tileBoard.GetTile(approxLocation);
		return closestParentTile == null ? null : closestParentTile.GetSubtileFromLocation(approxLocation);
	}

	public void RegisterMarker(Marker newMarker) {
		placedMarkers.Add(newMarker);
	}

	#endregion

	#region Private Functions

	private void AttemptPersistantDataRetrieval() {
		TryToUsePersistantTileSet();
		TryToUsePersistantPlayers();
	}

	private void TryToUsePersistantMarkerLimit() {
		this.MarkerLimit = PersistantGameData.Instance.numMarkers;
		if (this.MarkerLimit <= 0) {
			PopulateDefaultMarkerLimit();
		}
	}

	private void PopulateDefaultMarkerLimit() {
		this.MarkerLimit = 6;
	}

	private void TryToUsePersistantTileSet() {
		this.CurrentTileSet = PersistantGameData.Instance.selectedTileSet;
		if (this.CurrentTileSet == null) {
			PopulateDefaultTileSet();
		}
	}

	private void PopulateDefaultTileSet() {
		this.CurrentTileSet = TileSet.GetDefaultTileSet();
	}

	private void TryToUsePersistantPlayers() {
		this.Players = PersistantGameData.Instance.Players;
		if (this.Players == null || this.Players.Count < 2) {
			PopulateDefaultPlayers(2);
		}
		CurrentPlayer = Players[0];
	}

	private void PopulateDefaultPlayers(int numPlayers) {
		Players = new List<Player>();
		for (int i = 0; i < numPlayers; i++) {
			Player newPlayer = new Player();
			newPlayer.playerName = "Player " + i;
			newPlayer.playerColor = Color.HSVToRGB(((float)i / (float)numPlayers), 0.8f, 0.7f);
			Players.Add(newPlayer);
		}
	}


	#endregion

}

