﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TilePlacer : MonoBehaviour
{
	//TODO: Should more info be passed with this delegate?
	public delegate void TilePlacement();
	public static event TilePlacement SendTilePlaceEvent;

	private GameEngine gameEngine;
	private TileBag tileBag;
	private Tile placementTile;

	#region Unity Functions

	public void Awake() {
		TagUtils.SafeTagOverride(this.gameObject, Tag.TilePlacer);
		gameEngine = TagUtils.GetTaggedObject(Tag.GameEngine).GetComponent<GameEngine>();
		tileBag = TagUtils.GetTaggedObject(Tag.TileBag).GetComponent<TileBag>();
	}

	public void Start() {
		Tile startingTile = tileBag.GetStartingTile();
		gameEngine.PlaceTileAt(startingTile, Vector3.zero);
	}

	public void Update() {
		MovePlacementMarker();
		CheckForInput();
	}

	#endregion

	#region Public Functions

	public void Enable() {
		if (placementTile == null) {
			placementTile = tileBag.GetTile();
		}
		this.enabled = true;
	}

	public void Disable() {
		if (placementTile != null) {
			Debug.LogWarning("Disabling TilePlacer before its placement tile has been set to null!");
		}
		this.enabled = false;
	}

	#endregion

	#region Private Functions

	private void MovePlacementMarker() {
		Vector3 mousePosition = GetNormalizedMousePosition();
		placementTile.transform.position = VectorUtils.SnapToMultiple(mousePosition, Tile.SUBTILES_PER_TILE);

		if (Input.GetKeyDown(KeyCode.Alpha1)) {
			Debug.Log("Mouse Position = " + mousePosition);
			Debug.Log("Snapped Position = " + VectorUtils.SnapToMultiple(mousePosition, Tile.SUBTILES_PER_TILE));
		}
	}

	private Vector3 GetNormalizedMousePosition() {
		Vector3 mousePosition = Input.mousePosition;
		mousePosition.z -= Camera.main.transform.position.z;
		mousePosition = Camera.main.ScreenToWorldPoint(mousePosition);
		return mousePosition;
	}

	private void CheckForInput() {
		if (Input.GetButtonDown("Confirm")) {
			PlaceTileAt(placementTile.transform.position);
		}
		if (Input.GetButtonDown("Rotate")) {
			placementTile.Rotate();
		}
	}

	private void PlaceTileAt(Vector3 location) {
		bool success = gameEngine.PlaceTileAt(placementTile, location);
		if (success) {
			OnPlacementSuccess();
		}
	}

	public void OnPlacementSuccess() {
		placementTile = null;

		if (SendTilePlaceEvent != null) {
			SendTilePlaceEvent();
		}
	}

	#endregion


}
