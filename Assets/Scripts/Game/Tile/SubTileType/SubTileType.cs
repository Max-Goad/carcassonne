﻿public abstract class SubTileType
{
	protected enum STT
	{
		NULL,
		CASTLE,
		CASTLE_WALL,
		ROAD,
		ROAD_END,
		GRASS,
		MANOR
	}

	#region Public Functions

	public static bool IsValidPlacementMatch(SubTileType one, SubTileType other) {
		return one.GetPlacementSTT() == other.GetPlacementSTT();
	}

	public static bool IsExactMatch(SubTileType one, SubTileType other) {
		return one.GetSTT() == other.GetSTT();
	}

	public abstract UnityEngine.Color GetColor();
	public virtual bool CanAllowMarkers() {
		return true;
	}

	#endregion

	protected abstract STT GetPlacementSTT();
	protected virtual STT GetSTT() {
		return GetPlacementSTT();
	}
}

