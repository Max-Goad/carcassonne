﻿public class GrassType : SubTileType
{
	public override UnityEngine.Color GetColor() {
		return UnityEngine.Color.HSVToRGB(0.33f, 0.8f, 0.8f);
	}
		
	protected override STT GetPlacementSTT() {
		return STT.GRASS;
	}
}

