﻿public class CastleType : SubTileType
{
	public override UnityEngine.Color GetColor() {
		return UnityEngine.Color.yellow;
	}

	protected override STT GetPlacementSTT() {
		return STT.CASTLE;
	}
}

