﻿public class NullType : SubTileType
{
	public override UnityEngine.Color GetColor() {
		return UnityEngine.Color.gray;
	}

	public override bool CanAllowMarkers() {
		return false;
	}

	protected override STT GetPlacementSTT() {
		return STT.NULL;
	}
}

