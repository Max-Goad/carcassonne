﻿public class ManorType : SubTileType
{
	public override UnityEngine.Color GetColor() {
		return UnityEngine.Color.white;
	}

	protected override STT GetPlacementSTT() {
		return STT.MANOR;
	}
}

