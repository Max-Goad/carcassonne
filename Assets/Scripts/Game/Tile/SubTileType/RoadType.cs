﻿public class RoadType : SubTileType
{
	public override UnityEngine.Color GetColor() {
		return UnityEngine.Color.HSVToRGB(0.6f, 0.8f, 0.8f);
	}

	protected override STT GetPlacementSTT() {
		return STT.ROAD;
	}
}

