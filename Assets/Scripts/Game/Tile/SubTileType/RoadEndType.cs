﻿public class RoadEndType : RoadType
{
	public override UnityEngine.Color GetColor() {
		return UnityEngine.Color.HSVToRGB(0.6f, 0.8f, 0.6f);
	}

	protected override STT GetSTT() {
		return STT.ROAD_END;
	}
}

