﻿public class CastleWallType : CastleType
{
	public override UnityEngine.Color GetColor() {
		return UnityEngine.Color.HSVToRGB(0.1f, 0.8f, 0.8f);
	}

	protected override STT GetSTT() {
		return STT.CASTLE_WALL;
	}
}

