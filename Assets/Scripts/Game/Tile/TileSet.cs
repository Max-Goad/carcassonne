﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TileSet
{
	public Dictionary<TileID,int> rawSet {
		get;
		private set;
	}

	public string name { get; set; }

	public TileSet(string name) {
		rawSet = new Dictionary<TileID, int>();
		foreach (TileID each in System.Enum.GetValues(typeof(TileID))) {
			rawSet.Add(each, 0);
		}
		this.name = name;
	}

	public void SetTileNum(TileID id, int num) {
		rawSet[id] = num;
	}

	public int GetTileNum(TileID id) {
		return rawSet[id];
	}

	public override string ToString() {
		string toString = "[TileSet \"" + name + "\"]: ";
		foreach (var each in rawSet) {
			toString += "(" + each.Key + "," + each.Value + ")";
		}
		return toString;
	}

	public static TileSet GetDefaultTileSet() {
		TileSet newSet = new TileSet("Default");
		foreach (TileID each in System.Enum.GetValues(typeof(TileID))) {
			newSet.SetTileNum(each, 1);
		}
		return newSet;
	}

}

[System.Serializable]
public class SerializableTileSet
{
	public List<TileEntry> entries;
	public string tileSetName;

	public SerializableTileSet() {
	}
	public SerializableTileSet(TileSet tileSet) {
		entries = new List<TileEntry>();
		foreach (var each in tileSet.rawSet) {
			entries.Add(new TileEntry(each.Key, each.Value));
		}
		this.tileSetName = tileSet.name;
	}

	public TileSet ToTileSet() {
		TileSet newTileSet = new TileSet(this.tileSetName);
		foreach (var each in this.entries) {
			newTileSet.SetTileNum(each.id, each.num);
		}
		return newTileSet;
	}
}

[System.Serializable]
public class TileEntry
{
	public TileID id;
	public int num;

	public TileEntry() {
	}
	public TileEntry(TileID id, int num) {
		this.id = id;
		this.num = num;
	}
}


