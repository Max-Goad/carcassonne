﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SubTile : MonoBehaviour
{
	private SubTileType type;
	private Tile parentTile;
	private Marker marker;

	public SubTileType Type {
		get { return type; }
		set {
			this.type = value;
			ChangeAppearance();
		}
	}

	public Tile ParentTile { 
		get { return parentTile; }
		set {
			this.parentTile = value;
			this.transform.SetParent(parentTile.transform);
		}
	}

	public Marker Marker {
		get { return marker; }
		set {
			this.marker = value;
			this.marker.subtilePlacedOn = this;
		}
	}

	public bool isEndPiece { get; set; }

	#region Public Functions

	public bool HasCompatibleTypeTo(SubTile other) {
		return true;
	}

	public bool CanAllowMarkers() {
		Debug.Log("Type " + this.Type + " Can Allow Markers? " + this.Type.CanAllowMarkers());
		return this.Type.CanAllowMarkers();
	}

	public bool HasMarker() {
		return marker != null;
	}

	#endregion

	#region Private Functions

	private void ChangeAppearance() {
		this.GetComponent<Renderer>().material.color = Type.GetColor();
	}

	#endregion

}
