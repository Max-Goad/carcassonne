﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
	public TileID id { get; set; }

	private SubTile[,] subtiles;

	public static int SUBTILES_PER_TILE = 5;
	private static float OFFSET = Tile.SUBTILES_PER_TILE / 2;

	#region Unity Functions

	public void Awake() {
		subtiles = new SubTile[SUBTILES_PER_TILE, SUBTILES_PER_TILE];
	}
	
	public void Update() {
		UpdateSubtilePositions();
	}

	#endregion

	#region Public Functions

	public void SetSubtile(SubTile subtile, int x, int y) {
		if (IsValidCoord(x) && IsValidCoord(y)) {
			subtile.transform.parent = this.transform;
			subtile.gameObject.name = GetSubTileName(x, y);
			subtiles[x, y] = subtile;
		}
	}

	public SubTile[] GetSubTilesOnEdge(CardinalDirection direction) {
		
		SubTile[] returnArray = new SubTile[SUBTILES_PER_TILE];

		for (int i = 0; i < SUBTILES_PER_TILE; i++) {
			switch (direction) {
			case CardinalDirection.NORTH:
				returnArray[i] = subtiles[i, SUBTILES_PER_TILE - 1];
				break;
			case CardinalDirection.EAST:
				returnArray[i] = subtiles[SUBTILES_PER_TILE - 1, i];
				break;
			case CardinalDirection.SOUTH:
				returnArray[i] = subtiles[i, 0];
				break;
			case CardinalDirection.WEST:
				returnArray[i] = subtiles[0, i];
				break;
			}
		}

		return returnArray;
	}

	public void Rotate() {
		SubTile[,] newArray = new SubTile[SUBTILES_PER_TILE, SUBTILES_PER_TILE];

		for (int i = 0; i < SUBTILES_PER_TILE; i++) {
			for (int j = 0; j < SUBTILES_PER_TILE; j++) {
				int k = (SUBTILES_PER_TILE - 1) - j;
				newArray[k, i] = subtiles[i, j];
				newArray[k, i].gameObject.name = GetSubTileName(k, i);
			}
		}

		subtiles = newArray;
	}

	public SubTile GetSubtileFromLocation(Vector3 location) {
		Vector3 relativeLocation = VectorUtils.SnapToMultiple(location - this.transform.position);
		/*
		Debug.Log("Location = " + location);
		Debug.Log("Relative Location = " + relativeLocation);
		Debug.Log("Finalized Location = " + new Vector3(Mathf.RoundToInt(relativeLocation.x + OFFSET),
		                                                Mathf.RoundToInt(relativeLocation.y + OFFSET),
		                                                0.0f));
		                                                */
		return subtiles[Mathf.RoundToInt(relativeLocation.x + OFFSET), 
		                Mathf.RoundToInt(relativeLocation.y + OFFSET)];
	}

	#endregion

	#region Private Functions

	private void UpdateSubtilePositions() {
		for (int i = 0; i < SUBTILES_PER_TILE; i++) {
			for (int j = 0; j < SUBTILES_PER_TILE; j++) {
				Vector3 newPosition = new Vector3(this.transform.position.x + i - OFFSET, 
				                                  this.transform.position.y + j - OFFSET, 
				                                  0);
				subtiles[i, j].transform.position = newPosition;
			}
		}
	}

	private bool IsValidCoord(int x) {
		return x >= 0 && x < SUBTILES_PER_TILE;
	}

	private string GetSubTileName(int x, int y) {
		return "SubTile(" + x + "," + y + ")";
	}

	#endregion

}
