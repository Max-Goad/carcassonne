﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileFactory : MonoBehaviour
{
	public Tile blankTilePrefab;
	public SubTile blankSubTilePrefab;

	#region Unity Functions

	public void Awake() {
		TagUtils.SafeTagOverride(this.gameObject, Tag.TileFactory);
	}

	#endregion

	#region Public Functions

	public Tile CreateTile(TileID id) {
		
		Tile newTile = Instantiate(blankTilePrefab);

		newTile.id = id;

		SetSubTiles(ref newTile);

		return newTile;
	}

	public SubTile CreateSubTile(SubTileType sType, Tile parent) {
		SubTile newSubTile = Instantiate(blankSubTilePrefab);
		newSubTile.Type = sType;
		newSubTile.ParentTile = parent;
		return newSubTile;
	}

	#endregion

	#region Private Functions

	private void SetSubTiles(ref Tile tile) {
		SubTileType[,] subTileLayout = GetSubTileLayoutFrom(tile.id);
		for (int i = 0; i < Tile.SUBTILES_PER_TILE; i++) {
			for (int j = 0; j < Tile.SUBTILES_PER_TILE; j++) {
				SubTile newSubTile = CreateSubTile(subTileLayout[i, j], tile);
				tile.SetSubtile(newSubTile, i, j);
			}
		}
	}

	private SubTileType[,] GetSubTileLayoutFrom(TileID id) {
		SubTileType[,] returnArray;


		//TODO: Fill out this switch statement
		//TODO 2: Replace switch statement with something better?
		switch (id) {
		case TileID.A:
			returnArray = FillWithType(new GrassType());
			returnArray[2, 2] = new ManorType();
			returnArray[2, 3] = new RoadEndType();
			returnArray[2, 4] = new RoadType();
			break;
		case TileID.B:
			returnArray = FillWithType(new GrassType());
			returnArray[2, 2] = new ManorType();
			break;
		case TileID.C:
			returnArray = FillWithType(new CastleType());
			//TODO: Shield?
			break;
		case TileID.D:
			returnArray = FillWithType(new GrassType());
			FillColumnWithType(ref returnArray, 2, new RoadType());
			FillColumnWithType(ref returnArray, 4, new CastleWallType());
			break;
		case TileID.E:
			returnArray = FillWithType(new GrassType());
			FillRowWithType(ref returnArray, 0, new CastleWallType());
			break;
		case TileID.F:
			//TODO: Shield?
		case TileID.G:
			returnArray = FillWithType(new GrassType());
			FillRowWithType(ref returnArray, 1, new CastleWallType());
			FillRowWithType(ref returnArray, 2, new CastleType());
			FillRowWithType(ref returnArray, 3, new CastleWallType());
			FillColumnWithType(ref returnArray, 0, new CastleType());
			FillColumnWithType(ref returnArray, 4, new CastleType());
			break;
		case TileID.H:
			returnArray = FillWithType(new GrassType());
			FillColumnWithType(ref returnArray, 0, new CastleWallType());
			FillColumnWithType(ref returnArray, 4, new CastleWallType());
			break;
		case TileID.I:
			returnArray = FillWithType(new GrassType());
			FillRowWithType(ref returnArray, 4, new CastleWallType());
			FillColumnWithType(ref returnArray, 4, new CastleWallType());
			break;
		case TileID.J:
			returnArray = FillWithType(new GrassType());
			FillRowWithType(ref returnArray, 0, new CastleWallType());
			returnArray[4, 2] = new RoadType();
			returnArray[3, 2] = new RoadType();
			returnArray[2, 2] = new RoadType();
			returnArray[2, 3] = new RoadType();
			returnArray[2, 4] = new RoadType();
			break;
		case TileID.K:
			returnArray = FillWithType(new GrassType());
			FillRowWithType(ref returnArray, 0, new CastleWallType());
			returnArray[0, 2] = new RoadType();
			returnArray[1, 2] = new RoadType();
			returnArray[2, 2] = new RoadType();
			returnArray[2, 3] = new RoadType();
			returnArray[2, 4] = new RoadType();
			break;
		case TileID.L:
			returnArray = FillWithType(new GrassType());
			FillRowWithType(ref returnArray, 0, new CastleWallType());
			FillRowWithType(ref returnArray, 2, new RoadType());
			returnArray[2, 3] = new RoadType();
			returnArray[2, 4] = new RoadType();
			returnArray[2, 2] = new RoadEndType();
			break;
		case TileID.M:
			//TODO: Shield?
		case TileID.N:
			returnArray = FillWithType(new GrassType());
			FillRowWithType(ref returnArray, 0, new CastleType());
			FillColumnWithType(ref returnArray, 0, new CastleType());
			returnArray[1, 1] = new CastleType();
			returnArray[2, 2] = new CastleWallType();
			returnArray[1, 2] = new CastleWallType();
			returnArray[1, 3] = new CastleWallType();
			returnArray[2, 1] = new CastleWallType();
			returnArray[3, 1] = new CastleWallType();
			break;
		case TileID.O:
			//TODO: Shield?
		case TileID.P:
			returnArray = FillWithType(new GrassType());
			FillRowWithType(ref returnArray, 0, new CastleType());
			FillColumnWithType(ref returnArray, 0, new CastleType());
			returnArray[1, 1] = new CastleType();
			returnArray[2, 2] = new CastleWallType();
			returnArray[1, 2] = new CastleWallType();
			returnArray[1, 3] = new CastleWallType();
			returnArray[2, 1] = new CastleWallType();
			returnArray[3, 1] = new CastleWallType();
			returnArray[4, 2] = new RoadType();
			returnArray[3, 2] = new RoadType();
			returnArray[3, 3] = new RoadType();
			returnArray[2, 3] = new RoadType();
			returnArray[2, 4] = new RoadType();
			break;
		case TileID.Q:
			//Shield?
		case TileID.R:
			returnArray = FillWithType(new CastleType());
			FillRowWithType(ref returnArray, 3, new CastleWallType());
			FillRowWithType(ref returnArray, 4, new GrassType());
			break;
		case TileID.S:
			//Shield?
		case TileID.T:
			returnArray = FillWithType(new CastleType());
			FillRowWithType(ref returnArray, 3, new CastleWallType());
			FillRowWithType(ref returnArray, 4, new GrassType());
			returnArray[2, 4] = new RoadType();
			break;
		case TileID.U:
			returnArray = FillWithType(new GrassType());
			FillColumnWithType(ref returnArray, 2, new RoadType());
			break;
		case TileID.V:
			returnArray = FillWithType(new GrassType());
			returnArray[0, 2] = new RoadType();
			returnArray[1, 2] = new RoadType();
			returnArray[2, 2] = new RoadType();
			returnArray[2, 3] = new RoadType();
			returnArray[2, 4] = new RoadType();
			break;
		case TileID.W:
			returnArray = FillWithType(new GrassType());
			FillRowWithType(ref returnArray, 2, new RoadType());
			returnArray[2, 2] = new RoadEndType();
			returnArray[2, 3] = new RoadType();
			returnArray[2, 4] = new RoadType();
			break;
		case TileID.X:
			returnArray = FillWithType(new GrassType());
			FillRowWithType(ref returnArray, 2, new RoadType());
			FillColumnWithType(ref returnArray, 2, new RoadType());
			returnArray[2, 2] = new RoadEndType();
			break;
		default:
			returnArray = FillWithType(new NullType());
			break;
		}

		CutOutCorners(ref returnArray);

		return returnArray;
	}

	private SubTileType[,] FillWithType(SubTileType sType) {
		SubTileType[,] returnArray = new SubTileType[Tile.SUBTILES_PER_TILE, Tile.SUBTILES_PER_TILE];
		for (int i = 0; i < Tile.SUBTILES_PER_TILE; i++) {
			for (int j = 0; j < Tile.SUBTILES_PER_TILE; j++) {
				returnArray[i, j] = sType;
			}
		}
		return returnArray;
	}

	private void FillRowWithType(ref SubTileType[,] array, int row, SubTileType type) {
		for (int i = 0; i < Tile.SUBTILES_PER_TILE; i++) {
			array[i, row] = type;
		}
	}

	private void FillColumnWithType(ref SubTileType[,] array, int column, SubTileType type) {
		for (int j = 0; j < Tile.SUBTILES_PER_TILE; j++) {
			array[column, j] = type;
		}
	}

	private void CutOutCorners(ref SubTileType[,] array) {
		array[0, 0] = new NullType();
		array[0, Tile.SUBTILES_PER_TILE - 1] = new NullType();
		array[Tile.SUBTILES_PER_TILE - 1, 0] = new NullType();
		array[Tile.SUBTILES_PER_TILE - 1, Tile.SUBTILES_PER_TILE - 1] = new NullType();
	}

	#endregion

}
