﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class TileDictionary : Dictionary<TileID, List<Tile>>
{
	public TileDictionary() {
		populateTileKeys();
	}

	#region Public Functions

	public void Add(Tile tile) {
		List<Tile> tileListForType = this[tile.id];
		tileListForType.Add(tile);
	}

	public Tile GetAndRemove(TileID type) {
		List<Tile> tileListForID = this[type];
		Tile result = tileListForID[0];
		if (result != null) {
			tileListForID.RemoveAt(0);
		}

		return result;
	}

	public int NonZeroValuesCount() {
		int nonZeroCount = 0;
		foreach (KeyValuePair<TileID, List<Tile>> eachKV in this) {
			if (eachKV.Value != null && eachKV.Value.Count > 0) {
				nonZeroCount++;
			}
		}
		return nonZeroCount;
	}


	#endregion

	#region Private Functions

	private void populateTileKeys() {
		foreach (TileID curr in Enum.GetValues(typeof(TileID))) {
			this.Add(curr, new List<Tile>());
		}
	}

	#endregion

}

