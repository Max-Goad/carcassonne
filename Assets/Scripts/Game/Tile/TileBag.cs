﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using TDKeyValuePair = System.Collections.Generic.KeyValuePair<TileID, System.Collections.Generic.List<Tile>>;

public class TileBag : MonoBehaviour
{
	public TileID startingTile = TileID.A;

	private GameState gameState;
	private TileFactory tileFactory;
	private TileDictionary tileDictionary;

	#region Unity Functions

	public void Awake() {
		TagUtils.SafeTagOverride(this.gameObject, Tag.TileBag);
		gameState = TagUtils.GetTaggedObject(Tag.GameState).GetComponent<GameState>();
		tileFactory = TagUtils.GetTaggedObject(Tag.TileFactory).GetComponent<TileFactory>();
		tileDictionary = new TileDictionary();

		PopulateTiles();
	}

	#endregion

	#region Public Functions

	public Tile GetStartingTile() {
		return tileFactory.CreateTile(startingTile);
	}

	public Tile GetTile() {
		int randomNumber = UnityEngine.Random.Range(0, tileDictionary.NonZeroValuesCount());

		foreach (TDKeyValuePair eachKV in tileDictionary) {
			if (eachKV.Value.Count < 1)
				continue;

			if (randomNumber-- == 0) {
				Tile returnTile = tileDictionary.GetAndRemove(eachKV.Key);
				returnTile.gameObject.SetActive(true);
				return returnTile;
			}
		}

		throw new UnityException("TileBag.getTile() never returned a tile! Was the random number larger than the tileCount range?");
	}

	public Tile GetTile(TileID id) {
		//TODO
		throw new NotImplementedException();
	}

	public bool HasTilesRemaining() {
		return tileDictionary.NonZeroValuesCount() > 0;
	}

	#endregion

	#region Private Functions

	private void PopulateTiles() {
		TileSet tileSet = gameState.CurrentTileSet;

		foreach (TileID each in System.Enum.GetValues(typeof(TileID))) {
			int numTiles = tileSet.GetTileNum(each);
			for (int i = 0; i < numTiles; i++) {
				CreateTile(each);
			}
		}
	}

	private void CreateTile(TileID id) {
		Tile newTile = tileFactory.CreateTile(id);
		newTile.gameObject.name = "Tile (" + id.ToString() + ")";
		newTile.transform.SetParent(this.transform);
		newTile.gameObject.SetActive(false);
		tileDictionary.Add(newTile);
	}

	#endregion

}
