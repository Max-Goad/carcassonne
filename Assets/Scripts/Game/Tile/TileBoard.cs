﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileBoard : MonoBehaviour
{
	private Dictionary<Vector3, Tile> placedTiles;

	#region Unity Functions

	public void Awake() {
		TagUtils.SafeTagOverride(this.gameObject, Tag.TileBoard);
		placedTiles = new Dictionary<Vector3, Tile>();
	}

	#endregion

	#region Public Functions

	public void PlaceTileAt(Tile newTile, Vector3 location) {
		VectorUtils.SnapToMultiple(ref location, Tile.SUBTILES_PER_TILE);
		newTile.transform.position = location;
		newTile.name = "Tile(" + location.x + "," + location.y + ")";
		newTile.transform.SetParent(this.transform);
		placedTiles.Add(location, newTile);
	}

	public Tile GetTile(Vector3 location) {
		VectorUtils.SnapToMultiple(ref location, Tile.SUBTILES_PER_TILE);
		Tile returnTile = null;
		placedTiles.TryGetValue(location, out returnTile);
		return returnTile;
	}

	public bool HasTileAt(Vector3 location) {
		VectorUtils.SnapToMultiple(ref location, Tile.SUBTILES_PER_TILE);
		return placedTiles.ContainsKey(location);
	}

	public bool HasAnyTiles() {
		return placedTiles.Count > 0;
	}

	#endregion

	#region Private Functions

	#endregion

}

