﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersistantGameData
{
	#region Singleton Data

	private static PersistantGameData instance;
	public static PersistantGameData Instance {
		get {
			if (instance == null) {
				instance = new PersistantGameData();
			}
			return instance;
		}
	}

	private PersistantGameData() {
		Players = new List<Player>();
	}

	#endregion

	public int numMarkers;
	public TileSet selectedTileSet;

	public List<Player> Players {
		get;
		private set;
	}

	public void AddPlayer(Player p) {
		Players.Add(p);
	}
}
