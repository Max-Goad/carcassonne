﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEngine : MonoBehaviour
{
	private GameState gameState;

	#region Unity Functions

	public void Awake() {
		TagUtils.SafeTagOverride(this.gameObject, Tag.GameEngine);
		gameState = TagUtils.GetTaggedObject(Tag.GameState).GetComponent<GameState>();
	}
	
	public void Update() {
		
	}

	#endregion

	#region Public Functions

	public bool PlaceTileAt(Tile newTile, Vector3 location) {
		if (IsValidTilePlacement(newTile, location)) {
			FinalizeTilePlacement(newTile, location);
			return true;
		}
		else {
			return false;
		}
	}

	public bool PlaceMarkerOn(Marker newMarker, SubTile subtile) {
		if (IsValidMarkerPlacement(newMarker, subtile)) {
			FinalizeMarkerPlacement(newMarker, subtile);
			return true;
		}
		else {
			return false;
		}
	}

	#endregion

	#region Private Functions

	private bool IsValidTilePlacement(Tile tile, Vector3 location) {
		if (!gameState.HasAnyTiles()) {
			return true;
		}
		else if (gameState.HasTileAt(location)) {
			Debug.Log("GameState already has a Tile at location " + location);
			return false;
		}

		Tile northTile = gameState.GetTile(location + VectorUtils.north); 
		Tile eastTile = gameState.GetTile(location + VectorUtils.east);  
		Tile southTile = gameState.GetTile(location + VectorUtils.south);  
		Tile westTile = gameState.GetTile(location + VectorUtils.west);

		if (northTile != null &&
		    !DoSubTilesMatch(tile.GetSubTilesOnEdge(CardinalDirection.NORTH), 
		                     northTile.GetSubTilesOnEdge(CardinalDirection.SOUTH))) {
			return false;
		}

		if (eastTile != null &&
		    !DoSubTilesMatch(tile.GetSubTilesOnEdge(CardinalDirection.EAST), 
		                     eastTile.GetSubTilesOnEdge(CardinalDirection.WEST))) {
			return false;
		}

		if (southTile != null &&
		    !DoSubTilesMatch(tile.GetSubTilesOnEdge(CardinalDirection.SOUTH), 
		                     southTile.GetSubTilesOnEdge(CardinalDirection.NORTH))) {
			return false;
		}

		if (westTile != null &&
		    !DoSubTilesMatch(tile.GetSubTilesOnEdge(CardinalDirection.WEST), 
		                     westTile.GetSubTilesOnEdge(CardinalDirection.EAST))) {
			return false;
		}
			
		return northTile != null || southTile != null || eastTile != null || westTile != null;
	}

	private bool DoSubTilesMatch(SubTile[] one, SubTile[] other) {
		for (int i = 0; i < Tile.SUBTILES_PER_TILE; i++) {
			if (SubTileType.IsValidPlacementMatch(one[i].Type, other[i].Type) == false) {
				//Debug.Log("Found mismatch! One: " + one[i].type + ", Other: " + other[i].type);
				return false;
			}
		}

		return true;
	}

	private void FinalizeTilePlacement(Tile newTile, Vector3 location) {
		gameState.PlaceTileAt(newTile, location);
	}

	private bool IsValidMarkerPlacement(Marker marker, SubTile subtile) {
		return subtile != null && subtile.CanAllowMarkers() && !subtile.HasMarker();
	}

	private void FinalizeMarkerPlacement(Marker marker, SubTile subtile) {
		gameState.RegisterMarker(marker);
		subtile.Marker = marker;
	}

	#endregion

}
