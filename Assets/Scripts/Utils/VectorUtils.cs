﻿using UnityEngine;
using System.Collections;

public class VectorUtils
{
	public static Vector3 north = new Vector3(0.0f, Tile.SUBTILES_PER_TILE, 0.0f);
	public static Vector3 east = new Vector3(Tile.SUBTILES_PER_TILE, 0.0f, 0.0f);
	public static Vector3 south = new Vector3(0.0f, -Tile.SUBTILES_PER_TILE, 0.0f);
	public static Vector3 west = new Vector3(-Tile.SUBTILES_PER_TILE, 0.0f, 0.0f);

	public static void SnapToMultiple(ref Vector3 input) {
		input.x = Mathf.Round(input.x);
		input.y = Mathf.Round(input.y);
		input.z = Mathf.Round(input.z);
	}

	public static Vector3 SnapToMultiple(Vector3 input) {
		input.x = Mathf.Round(input.x);
		input.y = Mathf.Round(input.y);
		input.z = Mathf.Round(input.z);
		return input;
	}

	public static void SnapToMultiple(ref Vector3 input, int multiple) {
		input.x = Mathf.Round(input.x / multiple) * multiple;
		input.y = Mathf.Round(input.y / multiple) * multiple;
		input.z = Mathf.Round(input.z / multiple) * multiple;
	}

	public static Vector3 SnapToMultiple(Vector3 input, int multiple) {
		input.x = Mathf.Round(input.x / multiple) * multiple;
		input.y = Mathf.Round(input.y / multiple) * multiple;
		input.z = Mathf.Round(input.z / multiple) * multiple;
		return input;
	}
}

