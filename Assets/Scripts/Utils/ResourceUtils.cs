﻿using UnityEngine;
using System.Collections;

public class ResourceUtils
{
	public static Object SafeLoadResource(string subPathToResource) {
		Object loadedResource = Resources.Load(subPathToResource);
		if (loadedResource == null) {
			throw new UnityException("Error attempting to load resource at subpath: " + subPathToResource);
		}
		return loadedResource;
	}
}

