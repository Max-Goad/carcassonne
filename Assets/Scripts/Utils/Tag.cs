﻿public enum Tag
{
	TilePlacer,
	TileFactory,
	TileBag,
	TileBoard,
	GameState,
	GameEngine,
	MarkerPlacer,
	MarkerFactory,
	MarkerBag,
	PersistantGameData
}

