﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class TagUtils
{
	public static GameObject GetTaggedObject(Tag t) {
		GameObject returnObject = GameObject.FindGameObjectWithTag(t.ToString());
		if (returnObject == null) {
			throw new MissingComponentException("Could not find an object with the tag: " + t.ToString());
		}
		else {
			return returnObject;
		}
	}

	public static void SafeTagOverride(GameObject obj, Tag tag) {
		if (!obj.CompareTag(tag.ToString())) {
			Debug.LogWarning(tag.ToString() + " object incorrectly tagged as " + obj.tag + " instead of " + tag.ToString() + ". Automatically overriding tag.");
			obj.tag = tag.ToString();
		}
	}
}

