﻿/* MinMaxRangeAttribute.cs
* by Eddie Cameron – For the public domain
*/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class MinMaxRange
{
	public float rangeStart, rangeEnd;

	public MinMaxRange(float start, float end) {
		this.rangeStart = start;
		this.rangeEnd = end;
	}

	public float GetRandomValue() {
		return Random.Range(rangeStart, rangeEnd);
	}
}