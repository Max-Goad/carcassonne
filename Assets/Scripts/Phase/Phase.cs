﻿using UnityEngine;

public abstract class Phase
{
	public delegate void PhaseTransition(Phase newPhase);
	public static event PhaseTransition OnPhaseTransition;

	public abstract void Enter();
	public abstract void Exit();

	protected virtual void ActivatePhaseTransition(Phase newPhase) {
		if (OnPhaseTransition != null)
			OnPhaseTransition(newPhase);
	}
}

