﻿using UnityEngine;
using System;
using System.Collections;

#pragma warning disable 0168 //TODO: Remove and deal with this better later

public class PlaceTilePhase : Phase
{
	private TilePlacer tilePlacer;

	#region Singleton Data

	private static PlaceTilePhase instance;
	public static PlaceTilePhase Instance {
		get {
			if (instance == null) {
				instance = new PlaceTilePhase();
			}
			return instance;
		}
	}

	private PlaceTilePhase() {
	}

	#endregion

	#region Override Functions

	public override void Enter() {
		Debug.Log("PlaceTilePhase - Enter");
		SubscribeToTilePlacementEvents();
		ActivateTilePlacer();
	}

	public override void Exit() {
		Debug.Log("PlaceTilePhase - Exit");
		DeactivateTilePlacer();
		UnsubscribeFromTilePlacementEvents();
	}

	#endregion

	#region Private Functions

	private void ActivateTilePlacer() {
		try {
			if (tilePlacer == null) {
				tilePlacer = TagUtils.GetTaggedObject(Tag.TilePlacer).GetComponent<TilePlacer>();
			}
		} catch (MissingComponentException e) {
			GameObject.Instantiate(ResourceUtils.SafeLoadResource("Prefabs/Game/TilePlacer"));
			tilePlacer = TagUtils.GetTaggedObject(Tag.TilePlacer).GetComponent<TilePlacer>();
		} finally {
			tilePlacer.Enable();
		}
	}

	private void DeactivateTilePlacer() {
		tilePlacer.Disable();
	}

	private void SubscribeToTilePlacementEvents() {
		TilePlacer.SendTilePlaceEvent += TilePlaced;
	}

	private void UnsubscribeFromTilePlacementEvents() {
		TilePlacer.SendTilePlaceEvent -= TilePlaced;
	}

	private void TilePlaced() {
		ActivatePhaseTransition(PlaceMarkerPhase.Instance);
	}

	#endregion
}

