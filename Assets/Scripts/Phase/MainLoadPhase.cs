﻿using UnityEngine;

public class MainLoadPhase : Phase
{
	#region Singleton Data

	private static MainLoadPhase instance;
	public static MainLoadPhase Instance {
		get {
			if (instance == null) {
				instance = new MainLoadPhase();
			}
			return instance;
		}
	}

	private MainLoadPhase() {
	}

	#endregion

	#region Override Functions

	public override void Enter() {
		Debug.Log("MainLoadPhase - Enter");
		CreateEssentialInstances();
		ActivatePhaseTransition(PlaceTilePhase.Instance);
	}

	public override void Exit() {
		Debug.Log("MainLoadPhase - Exit");
	}

	#endregion

	#region Private Functions

	private void CreateEssentialInstances() {
		GameObject.Instantiate(ResourceUtils.SafeLoadResource("Prefabs/Game/Essentials"));
		//TODO: What's it's name? Where does it go? Does any of it matter? Find out, next time...
	}

	#endregion
}

