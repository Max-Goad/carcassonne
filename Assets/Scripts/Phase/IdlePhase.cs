﻿using UnityEngine;

public class IdlePhase : Phase
{
	#region Singleton Data

	private static IdlePhase instance;
	public static IdlePhase Instance {
		get {
			if (instance == null) {
				instance = new IdlePhase();
			}
			return instance;
		}
	}

	private IdlePhase() {
	}

	#endregion

	public override void Enter() {
		Debug.Log("IdlePhase - Enter");
	}
	public override void Exit() {
		Debug.Log("IdlePhase - Exit");
	}
}

