﻿using UnityEngine;

public class PhaseMachine : MonoBehaviour
{
	//TODO: Replace this with an actual phase
	public Phase startingPhase = MainLoadPhase.Instance;
	public Phase currentPhase {
		get;
		private set;
	}

	#region Unity Functions

	public void OnEnable() {
		currentPhase = startingPhase;
		Phase.OnPhaseTransition += ChangePhase;
		currentPhase.Enter();
	}

	public void OnDisable() {
		Phase.OnPhaseTransition -= ChangePhase;
	}

	#endregion

	#region Public Functions

	public void ChangePhase(Phase newPhase) {
		currentPhase.Exit();
		currentPhase = newPhase;
		currentPhase.Enter();
	}

	#endregion

	#region Private Functions

	#endregion

}

