﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#pragma warning disable 0168 //TODO: Remove and deal with this better later

public class PlaceMarkerPhase : Phase
{
	private MarkerPlacer markerPlacer;

	#region Singleton Data

	private static PlaceMarkerPhase instance;
	public static PlaceMarkerPhase Instance {
		get {
			if (instance == null) {
				instance = new PlaceMarkerPhase();
			}
			return instance;
		}
	}

	private PlaceMarkerPhase() {
	}

	#endregion

	#region Override Functions

	public override void Enter() {
		Debug.Log("PlaceMarkerPhase - Enter");
		SubscribeToMarkerPlacementEvents();
		ActivateMarkerPlacer();
	}

	public override void Exit() {
		Debug.Log("PlaceMarkerPhase - Exit");
		DeactivateMarkerPlacer();
		UnsubscribeFromMarkerPlacementEvents();
	}

	#endregion

	#region Private Functions

	private void ActivateMarkerPlacer() {
		try {
			if (markerPlacer == null) {
				markerPlacer = TagUtils.GetTaggedObject(Tag.MarkerPlacer).GetComponent<MarkerPlacer>();
			}
		} catch (MissingComponentException e) {
			GameObject.Instantiate(ResourceUtils.SafeLoadResource("Prefabs/Game/MarkerPlacer"));
			markerPlacer = TagUtils.GetTaggedObject(Tag.MarkerPlacer).GetComponent<MarkerPlacer>();
		} finally {
			markerPlacer.Enable();
		}
	}

	private void DeactivateMarkerPlacer() {
		markerPlacer.Disable();
	}

	private void SubscribeToMarkerPlacementEvents() {
		MarkerPlacer.SendMarkerPlaceEvent += MarkerPlaced;
	}

	private void UnsubscribeFromMarkerPlacementEvents() {
		MarkerPlacer.SendMarkerPlaceEvent -= MarkerPlaced;
	}

	private void MarkerPlaced() {
		TileBag tileBag = TagUtils.GetTaggedObject(Tag.TileBag).GetComponent<TileBag>();
		if (tileBag.HasTilesRemaining()) {
			GameState gameState = TagUtils.GetTaggedObject(Tag.GameState).GetComponent<GameState>();
			gameState.IncrementTurn();
			ActivatePhaseTransition(PlaceTilePhase.Instance);
		}
		else {
			//TODO: Create EndGamePhase and possible phases in between for scoring, etc
			//ActivatePhaseTransition(EndGamePhase.Instance);
		}
	}

	#endregion

}
