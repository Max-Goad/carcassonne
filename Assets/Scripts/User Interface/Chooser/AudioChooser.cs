﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioChooser : OptionChooser
{
	protected override void PopulateOptions() {
		for (int i = 0; i <= 100; i += 10) {
			options.Add(new AudioOption(i));
		}
	}

}

