﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TilesetChooser : OptionChooser
{
	protected override void PopulateOptions() {
		List<TileSet> tilesets = TileSetSerializer.Instance.LoadAllTileSets();
		foreach (TileSet each in tilesets) {
			options.Add(new TilesetOption(each));
		}
	}
}

