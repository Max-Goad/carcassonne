﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarkerNumberChooser : OptionChooser
{
	protected override void PopulateOptions() {
		//TODO: Replace min/max with actual lookups
		for (int i = 4; i <= 8; i++) {
			options.Add(new MarkerNumberOption(i));
		}
	}

}

