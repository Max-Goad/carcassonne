﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorChooser : OptionChooser
{
	protected override void PopulateOptions() {
		options.Add(new ColorOption(Color.black, "Black"));
		options.Add(new ColorOption(Color.blue, "Blue"));
		options.Add(new ColorOption(Color.red, "Red"));
		options.Add(new ColorOption(Color.yellow, "Yellow"));
		options.Add(new ColorOption(Color.green, "Green"));
		options.Add(new ColorOption(Color.white, "White"));
	}

	public Color GetCurrentColor() {
		ColorOption c = (ColorOption)this.options[currentOption];
		return c.GetColor();
	}

	protected override int GetDefaultOptionNumber() {
		return Random.Range(0, options.Count);
	}

}
