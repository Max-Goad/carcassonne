﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindowChooser : OptionChooser
{
	protected override void PopulateOptions() {
		options.Add(new WindowOption(true));
		options.Add(new WindowOption(false));
	}

}
