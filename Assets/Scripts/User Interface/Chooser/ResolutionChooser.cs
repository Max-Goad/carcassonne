﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResolutionChooser : OptionChooser
{
	protected override void PopulateOptions() {
		options.Add(new ResolutionOption(640, 360));
		options.Add(new ResolutionOption(640, 480));
		options.Add(new ResolutionOption(1024, 576));
		options.Add(new ResolutionOption(1024, 768));
		options.Add(new ResolutionOption(1280, 720));
		options.Add(new ResolutionOption(1280, 960));
		options.Add(new ResolutionOption(1600, 900));
		options.Add(new ResolutionOption(1920, 1080));
	}

}
