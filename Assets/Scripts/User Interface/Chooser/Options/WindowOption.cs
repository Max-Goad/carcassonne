﻿using UnityEngine;
using System.Collections;

public class WindowOption : Option
{
	bool fullscreen;

	public WindowOption(bool fullscreen) {
		this.fullscreen = fullscreen;
		this.NameString = fullscreen ? "Fullscreen" : "Windowed";
	}

	public override void Apply() {
		Screen.SetResolution(Screen.width, Screen.height, fullscreen);
	}
}

