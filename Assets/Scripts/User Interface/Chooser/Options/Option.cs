﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Option
{
	public string NameString {
		get;
		protected set;
	}

	public abstract void Apply();
}
