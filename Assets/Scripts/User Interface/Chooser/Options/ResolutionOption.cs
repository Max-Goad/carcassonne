﻿using UnityEngine;
using System.Collections;

public class ResolutionOption : Option
{
	private int width, height;

	public ResolutionOption(int width, int height) {
		this.width = width;
		this.height = height;
		this.NameString = CreateResolutionString();
	}

	public override void Apply() {
		Screen.SetResolution(width, height, Screen.fullScreen);
	}

	private string CreateResolutionString() {
		string resolution = width + " x " + height;
		string aspectRatio;
		float ratio = (float)width / (float)height;
		if (ratio == 16.0f / 9.0f) {
			aspectRatio = "(16:9)";
		}
		else if (ratio == 4.0f / 3.0f) {
			aspectRatio = "(4:3)";
		}
		else {
			aspectRatio = "(??:?)";
		}
		return resolution + " " + aspectRatio;
	}
}

