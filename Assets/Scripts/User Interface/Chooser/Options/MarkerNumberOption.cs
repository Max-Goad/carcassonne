﻿using UnityEngine;
using System.Collections;

public class MarkerNumberOption : Option
{
	private int markerNum;

	public MarkerNumberOption(int markerNum) {
		this.markerNum = markerNum;
		this.NameString = markerNum.ToString() + " Markers";
	}

	public override void Apply() {
		PersistantGameData.Instance.numMarkers = markerNum;
	}
}

