﻿using UnityEngine;
using System.Collections;

public class TilesetOption : Option
{
	private TileSet tileset;

	public TilesetOption(TileSet tileset) {
		this.tileset = tileset;
		this.NameString = tileset.name;
	}

	public override void Apply() {
		PersistantGameData.Instance.selectedTileSet = tileset;
	}
}

