﻿using UnityEngine;
using System.Collections;

public class ColorOption : Option
{
	private Color color;

	public ColorOption(Color color, string colorName) {
		this.color = color;
		this.NameString = colorName;
	}

	public override void Apply() {
		//TODO: Apply to Player Object
	}

	public Color GetColor() {
		return this.color;
	}
}