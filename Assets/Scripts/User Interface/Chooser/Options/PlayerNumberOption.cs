﻿using UnityEngine;
using System.Collections;

public class PlayerNumberOption : Option
{
	private int playerNum;

	public PlayerNumberOption(int playerNum) {
		this.playerNum = playerNum;
		this.NameString = playerNum.ToString() + " Players";
	}

	public override void Apply() {
		//PersistantGameData.Instance.numPlayers = playerNum;
	}
}

