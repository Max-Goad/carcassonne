﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class OptionChooser : MonoBehaviour
{
	public Text selectionText;

	protected List<Option> options;
	protected int currentOption = -1;

	public void Awake() {
		options = new List<Option>();
		PopulateOptions();
		Reset();
	}

	protected abstract void PopulateOptions();

	#region Public Functions

	public void LeftButtonPressed() {
		if (--currentOption < 0) {
			currentOption = 0;
			return;
		}
		else {
			UpdateSelectionText();
		}
	}

	public void RightButtonPressed() {
		if (++currentOption >= options.Count) {
			currentOption = options.Count - 1;
			return;
		}
		else {
			UpdateSelectionText();
		}
	}

	public void ApplyButtonPressed() {
		#if UNITY_EDITOR
		Debug.Log("Apply Button Pressed For " + this.ToString());
		#endif
		options[currentOption].Apply();
	}

	public void Reset() {
		currentOption = GetDefaultOptionNumber();
		UpdateSelectionText();
	}

	#endregion

	protected virtual int GetDefaultOptionNumber() {
		return options.Count - 1;
	}

	private void UpdateSelectionText() {
		selectionText.text = options[currentOption].NameString;
	}

}
