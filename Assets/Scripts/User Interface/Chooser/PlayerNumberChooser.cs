﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerNumberChooser : OptionChooser
{
	protected override void PopulateOptions() {
		//TODO: Replace min/max with actual lookups
		for (int i = 2; i <= 6; i++) {
			options.Add(new PlayerNumberOption(i));
		}
	}

}

