﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AddPlayerButton : MonoBehaviour
{
	public Text playerNameText;
	public ColorChooser colorChooser;
	public PlayerReference playerReference;

	public void OnClick() {
		playerReference.UpdatePlayerData(playerNameText.text, colorChooser.GetCurrentColor());
	}

}
