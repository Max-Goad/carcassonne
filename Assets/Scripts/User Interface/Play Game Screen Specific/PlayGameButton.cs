﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayGameButton : MonoBehaviour
{
	public PlayerPanelManager playerManager;
	public MarkerNumberChooser markerNumberChooser;
	public TilesetChooser tileSetChooser;


	#region Unity Functions

	public void OnEnable() {
		this.GetComponent<Button>().interactable = playerManager.GetValidPlayerNum() >= 2;
	}

	public void OnClick() {
		PersistPlayerData();
		PersistMarkerData();
		PersistTileSetData();

		SceneManager.LoadScene("GameScene");
	}

	#endregion

	#region Private Functions

	private void PersistPlayerData() {
		Player[] playerData = playerManager.GetValidPlayerData();
		foreach (Player each in playerData) {
			PersistantGameData.Instance.AddPlayer(each);
		}
	}

	private void PersistMarkerData() {
		markerNumberChooser.ApplyButtonPressed();
	}

	private void PersistTileSetData() {
		tileSetChooser.ApplyButtonPressed();
	}

	#endregion
}
