﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerPanel : MonoBehaviour
{
	public Button button;
	public PlayerMonoBehavior playerRepresentation;

	public PlayerReference reference;

	public void ActivateButton() {
		button.gameObject.SetActive(true);
		playerRepresentation.gameObject.SetActive(false);
	}

	public void ActivatePlayer() {
		playerRepresentation.gameObject.SetActive(true);	
		button.gameObject.SetActive(false);	
	}

	public void Deactivate() {
		button.gameObject.SetActive(false);
		playerRepresentation.gameObject.SetActive(false);
	}

	public void UpdatePlayerReference() {
		reference.UpdatePlayerReference(ref playerRepresentation);
	}

}
