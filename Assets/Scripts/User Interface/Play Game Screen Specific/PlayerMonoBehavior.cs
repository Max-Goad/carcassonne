﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMonoBehavior : MonoBehaviour
{
	public string playerName = null;
	public Color playerColor = Color.clear;

	public bool FieldsHaveBeenSet() {
		return (playerName != null) && (playerColor != Color.clear);
	}

	public Player ToPlayer() {
		Player newPlayer = new Player();
		newPlayer.playerName = this.playerName;
		newPlayer.playerColor = this.playerColor;
		return newPlayer;
	}
}