﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerRepresentationText : MonoBehaviour
{
	public PlayerMonoBehavior player;
	public Text text;

	public void OnEnable() {
		text.text = player.playerName;
		text.color = player.playerColor;
	}

}
