﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerReference : MonoBehaviour
{
	private PlayerMonoBehavior playerReference;

	#region Unity Functions

	public void Awake() {
		
	}
	
	public void Update() {
		
	}

	#endregion

	#region Public Functions

	public void UpdatePlayerReference(ref PlayerMonoBehavior playerReference) {
		this.playerReference = playerReference;
	}

	public void UpdatePlayerData(string playerName, Color playerColor) {
		this.playerReference.playerName = playerName;
		this.playerReference.playerColor = playerColor;
	}

	#endregion

	#region Private Functions

	#endregion

}
