﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPanelManager : MonoBehaviour
{
	public PlayerPanel[] playerPanels;

	public void OnEnable() {
		bool complete = false;
		foreach (PlayerPanel each in playerPanels) {
			if (!complete) {
				if (each.playerRepresentation.FieldsHaveBeenSet()) {
					each.ActivatePlayer();
				}
				else {
					each.ActivateButton();
					complete = true;
				}
			}
			else {
				each.Deactivate();
			}
		}
	}

	public int GetValidPlayerNum() {
		int count = 0;
		foreach (PlayerPanel each in playerPanels) {
			if (each.playerRepresentation.FieldsHaveBeenSet()) {
				count++;
			}
		}
		return count;
	}

	public Player[] GetValidPlayerData() {
		List<Player> playerData = new List<Player>();
		foreach (PlayerPanel each in playerPanels) {
			if (each.playerRepresentation.FieldsHaveBeenSet()) {
				playerData.Add(each.playerRepresentation.ToPlayer());
			}
		}
		return playerData.ToArray();
	}
}
