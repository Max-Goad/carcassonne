﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuStack : MonoBehaviour
{
	public GameObject initialObject;

	private Stack<GameObject> menuStack;

	#region Unity Functions

	public void Awake() {
		menuStack = new Stack<GameObject>();
		AddToStack(initialObject);
	}
	
	public void Update() {
		
	}

	#endregion

	#region Public Functions

	public void AddToStack(GameObject newTop) {
		if (menuStack.Count > 0) {
			GameObject currentTop = menuStack.Peek();
			if (currentTop != null) {
				currentTop.SetActive(false);
			}
		}

		menuStack.Push(newTop);
		if (newTop != null) {
			newTop.SetActive(true);
		}
	}

	public void PopStack() {
		if (menuStack.Count <= 1) {
			return;
		}

		GameObject oldTop = menuStack.Pop();
		if (oldTop != null) {
			oldTop.SetActive(false);
		}

		GameObject newTop = menuStack.Peek(); 
		if (newTop != null) {
			newTop.SetActive(true);			
		}
	}

	#endregion

	#region Private Functions

	#endregion

}
