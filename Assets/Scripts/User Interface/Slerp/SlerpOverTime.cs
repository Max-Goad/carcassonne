﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlerpOverTime : MonoBehaviour
{
	[Range(0.1f, 10.0f)]
	public float slerpSpeed = 1.0f;

	private static float lowerSlerpBound = 0.0f;
	private static float upperSlerpBound = 1.0f;
	private float currentSlerpValue;
	private float goalSlerpValue;

	#region Unity Functions

	public void OnEnable() {
		this.currentSlerpValue = lowerSlerpBound;
		this.goalSlerpValue = lowerSlerpBound;
	}

	#endregion

	#region Public Functions

	public float GetCurrentSlerpValue() {
		return currentSlerpValue;
	}

	public void ToUpperBound() {
		goalSlerpValue = upperSlerpBound;
		StartCoroutine("Slerp");
	}

	public void ToLowerBound() {
		goalSlerpValue = lowerSlerpBound;
		StartCoroutine("Slerp");
	}

	#endregion

	#region Private Functions

	private IEnumerator Slerp() {
		float startingValue = currentSlerpValue;
		float elapsedTime = 0.0f;
		while (currentSlerpValue != goalSlerpValue) {
			elapsedTime += Time.deltaTime * slerpSpeed;
			currentSlerpValue = Mathf.SmoothStep(startingValue, goalSlerpValue, elapsedTime);
			yield return new WaitForEndOfFrame();
		}
	}

	#endregion
}
