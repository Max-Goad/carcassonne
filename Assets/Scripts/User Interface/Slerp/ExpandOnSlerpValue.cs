﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExpandOnSlerpValue : MonoBehaviour
{
	public SlerpOverTime slerp;

	[Range(-1.0f, 1.0f)]
	public float expandSize = 0.2f;

	private Vector3 startingScale;

	public void Awake() {
		startingScale = this.transform.localScale;
	}

	public void Update() {
		this.transform.localScale = startingScale * (1.0f + expandSize * slerp.GetCurrentSlerpValue());
	}

}
