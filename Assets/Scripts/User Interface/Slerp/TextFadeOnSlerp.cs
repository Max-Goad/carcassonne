﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextFadeOnSlerp : MonoBehaviour
{
	public SlerpOverTime slerp;
	public Text text;

	public void Update() {
		Color c = text.color;
		c.a = slerp.GetCurrentSlerpValue();
		text.color = c;
	}

}
